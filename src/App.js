import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Login from "./pages/login";
import Dashboard from "./pages/dashboard";
import Index from "./pages/index";
import AddPage from "./pages/add";
import EditPage from "./pages/edit";
import Register from "./pages/register";
import NotFound from "./pages/notfound";
import FileUploadPage from "./pages/fileupload";
import Brand from "./pages/list/brand";
import Bank from "./pages/list/bank";
import Customers from "./pages/list/customers";
import Gudang from "./pages/list/gudang";
import Kategori from "./pages/list/kategori";
import Pasar from "./pages/list/pasar";
import Produk from "./pages/list/produk";
import Sales from "./pages/list/sales";
import Satuan from "./pages/list/satuan";
import Supplier from "./pages/list/supplier";
import TipeBayar from "./pages/list/tipeBayar";
import TipeProduk from "./pages/list/tipeProduk";

import AddBank from "./pages/tambah/add_bank";
import AddBrand from "./pages/tambah/add_brand";
import AddCustomers from "./pages/tambah/add_customers";
import AddGudang from "./pages/tambah/add_gudang";
import AddPasar from "./pages/tambah/add_pasar";
// import AddProduk from "./pages/tambah/produk";
import AddSales from "./pages/tambah/add_sales";
import AddSupplier from "./pages/tambah/add_supplier";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <Switch>
            <Route exact path="/" component={Login} />
            <Route path="/dashboard" component={Dashboard} />
            <Route path="/brand" component={Brand} />
            <Route path="/bank" component={Bank} />
            <Route path="/customers" component={Customers} />
            <Route path="/gudang" component={Gudang} />
            <Route path="/kategori" component={Kategori} />
            <Route path="/pasar" component={Pasar} />
            <Route path="/produk" component={Produk} />
            <Route path="/sales" component={Sales} />
            <Route path="/satuan" component={Satuan} />
            <Route path="/supplier" component={Supplier} />
            <Route path="/tipeBayar" component={TipeBayar} />
            <Route path="/tipeProduk" component={TipeProduk} />
            <Route path="/index" component={Index} />
            <Route path="/register" component={Register} />

            <Route path="/add_bank" component={AddBank} />
            <Route path="/add_brand" component={AddBrand} />
            <Route path="/add_customers" component={AddCustomers} />
            <Route path="/add_gudang" component={AddGudang} />
            <Route path="/add_pasar" component={AddPasar} />
            {/* <Route path="/add_produk" component={AddProduk} /> */}
            <Route path="/add_sales" component={AddSales} />
            <Route path="/add_supplier" component={AddSupplier} />

            <Route path="/add" component={AddPage} />
            <Route path="/edit/" component={EditPage} />
            <Route path="/fileupload/" component={FileUploadPage} />
            <Route path="*" component={NotFound} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
