import React, { Component } from "react";
import Header from "../../elements/header";
import Sidebar from "../../elements/sidebar";
import { Link, Redirect } from "react-router-dom";
import axios from "axios";

export default class AddPage extends Component {
  state = {
    redirect: false
  };

  handleSubmit = event => {
    event.preventDefault();
    this.setState({ isLoading: true });
    const url = "http://localhost/YII/distribution/api/v1/master/supplier/new";
    const accToken = localStorage.getItem("token");
    const stringed = accToken.replace(/"/gi, "");
    console.log(stringed);

    const email = document.getElementById("inputEmail").value;
    const nama = document.getElementById("inputName").value;
    const alamat = document.getElementById("inputAddress").value;
    const telepon = document.getElementById("inputContact").value;

    let bodyFormData = new FormData();
    bodyFormData.set("nama", nama);
    bodyFormData.set("email", email);
    bodyFormData.set("alamat", alamat);
    bodyFormData.set("telepon", telepon);

    axios
      .post(url, bodyFormData, {
        headers: {
          Authorization: `Bearer ${stringed}`
        }
      })
      .then(result => {
        if (result.data.info.status) {
          this.setState({ redirect: true, isLoading: false });
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/supplier" />;
    }
  };

  render() {
    return (
      <div>
        <Header />
        <div id="wrapper">
          <Sidebar></Sidebar>
          <div id="content-wrapper">
            <div className="container-fluid">
              <ol className="breadcrumb">
                <li className="breadcrumb-item">
                  <Link to={"/dashboard"}>Dashboard</Link>
                </li>
                <li className="breadcrumb-item">
                  <Link to={"/supplier"}>Supplier</Link>
                </li>
                <li className="breadcrumb-item active">Tambah Supplier</li>
              </ol>
            </div>
            <div className="container-fluid">
              <div className="card mx-auto">
                <div className="card-header">Tambah Supplier</div>
                <div className="card-body">
                  <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                      <div className="form-row">
                        <div className="col-md-6">
                          <div className="form-label-group">
                            <input
                              type="text"
                              id="inputName"
                              className="form-control"
                              placeholder="Enter name"
                              required="required"
                              autoFocus="autofocus"
                            />
                            <label htmlFor="inputName">
                              Masukkan Nama Supplier
                            </label>
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-label-group">
                            <input
                              type="text"
                              id="inputEmail"
                              className="form-control"
                              placeholder="Enter name"
                              required="required"
                            />
                            <label htmlFor="inputEmail">
                              Masukkan Email Supplier
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="form-group">
                      <div className="form-row">
                        <div className="col-md-6">
                          <div className="form-label-group">
                            <input
                              type="text"
                              id="inputAddress"
                              className="form-control"
                              placeholder="Enter name"
                              required="required"
                            />
                            <label htmlFor="inputAddress">
                              Masukkan Alamat Supplier
                            </label>
                          </div>
                        </div>
                        <div className="col-md-6">
                          <div className="form-label-group">
                            <input
                              type="text"
                              id="inputContact"
                              className="form-control"
                              placeholder="Enter name"
                              required="required"
                            />
                            <label htmlFor="inputContact">
                              Masukkan Kontak Supplier
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <button
                      className="btn btn-primary btn-block"
                      type="submit"
                      disabled={this.state.isLoading ? true : false}
                    >
                      Tambah Data &nbsp;&nbsp;&nbsp;
                    </button>
                  </form>
                  {this.renderRedirect()}
                </div>
              </div>
            </div>

            <footer className="sticky-footer">
              <div className="container my-auto">
                <div className="copyright text-center my-auto">
                  <span>
                    Copyright © Your Website{" "}
                    <div>{new Date().getFullYear()}</div>
                  </span>
                </div>
              </div>
            </footer>
          </div>
        </div>
      </div>
    );
  }
}
