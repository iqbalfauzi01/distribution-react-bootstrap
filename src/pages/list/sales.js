import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import Header from "../../elements/header";
import Sidebar from "../../elements/sidebar";

export default class Bank extends Component {
  constructor(props) {
    super(props);
    // this.url =
    //   "http://distribution.teamdhuar.com/distribution/api/v1/master/sales/list";
    this.url = "http://localhost/YII/distribution/api/v1/master/sales/list";
    this.state = {
      brand: []
    };
  }

  state = {
    redirect: false,
    isLoading: false
  };

  componentDidMount() {
    const accToken = localStorage.getItem("token");
    const stringed = accToken.replace(/"/gi, "");
    console.log(stringed);

    axios(this.url, {
      method: "GET",
      responseType: "json",
      cache: false,
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/json",
        Authorization: `Bearer ${stringed}`
      }
    })
      .then(res => {
        const brand = res.data;
        this.setState({ brand });
      })
      .catch(error => {
        console.log(error);
        console.log("salah");
      });
  }
  render() {
    return (
      <div>
        <Header />
        <div id="wrapper">
          <Sidebar></Sidebar>
          <div id="content-wrapper">
            <div className="container-fluid">
              <ol className="breadcrumb">
                <li className="breadcrumb-item">
                  <Link to={"/dashboard"}>Dashboard</Link>
                </li>
                <li className="breadcrumb-item active">Sales</li>
              </ol>

              <div className="card mb-3">
                <div className="card-header">
                  <i className="fas fa-table"></i>
                  &nbsp;Sales List
                </div>
                <div className="card-body">
                  <div className="table-responsive">
                    <table
                      className="table table-bordered"
                      id="dataTable"
                      width="100%"
                      cellSpacing="0"
                    >
                      <thead align="center">
                        <tr>
                          <th>Id</th>
                          <th>Kode</th>
                          <th>Nama</th>
                          <th>Alamat</th>
                          <th>Contact Person</th>
                          <th>Option</th>
                        </tr>
                      </thead>
                      <tbody>
                        {this.state.brand.map((p, index) => {
                          return (
                            <tr key={index}>
                              <td component="th" scope="row" align="center">
                                {p.id}
                              </td>
                              <td align="left">{p.kode}</td>
                              <td align="center">{p.nama}</td>
                              <td align="center">{p.alamat}</td>
                              <td align="center">{p.contact_person}</td>
                              <td></td>
                            </tr>
                          );
                        })}
                      </tbody>
                    </table>
                    <span>
                      <Link class="badge badge-primary" to="/add_sales">
                        Tambah
                      </Link>
                    </span>
                  </div>
                </div>
                <div className="card-footer small text-muted">
                  {/* Updated yesterday at 11:59 PM */}
                </div>
              </div>
            </div>

            <footer className="sticky-footer">
              <div className="container my-auto">
                <div className="copyright text-center my-auto">
                  <span>Copyright © Your Website 2020</span>
                </div>
              </div>
            </footer>
          </div>
        </div>
      </div>
    );
  }
}
