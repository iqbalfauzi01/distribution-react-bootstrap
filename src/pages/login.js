import React, { Component } from "react";
import axios from "axios";
import { Link, Redirect } from "react-router-dom";
import TitleComponent from "./title";

export default class Login extends Component {
  state = {
    redirect: false,
    authError: false,
    isLoading: false
  };

  // handleEmailChange = event => {
  //   this.setState({ email: event.target.value });
  // };
  // handlePwdChange = event => {
  //   this.setState({ password: event.target.value });
  // };

  handleSubmit = event => {
    event.preventDefault();
    this.setState({ isLoading: true });
    const url = "http://localhost/YII/distribution/api/v1/login";
    axios
      .post(url, {
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*"
        },
        responseType: "json",
        cache: false,
        crossOriginLoading: "use-credentials",
        mode: "no-cors",
        LoginForm: {
          login: this.state.login,
          password: this.state.password
        }
      })
      .then(response => {
        console.log(response.data);
        if (response.data.info.status) {
          localStorage.setItem("token", response.data.info.access_token);
          this.setState({ redirect: true, isLoading: false });
        }
      })
      .catch(error => {
        console.log(error);
        this.setState({ authError: true, isLoading: false });
      });
    console.log(this.state.login);
    console.log(this.state.password);
  };

  renderRedirect = () => {
    if (this.state.redirect) {
      return <Redirect to="/dashboard" />;
    }
  };

  render() {
    const isLoading = this.state.isLoading;
    return (
      <div className="container">
        <TitleComponent title="React CRUD Login "></TitleComponent>
        <div className="card card-login mx-auto mt-5">
          <div className="card-header">Login</div>
          <div className="card-body">
            <form onSubmit={this.handleSubmit}>
              <div className="form-group">
                <div className="form-label-group">
                  <input
                    className={
                      "form-control " +
                      (this.state.authError ? "is-invalid" : "")
                    }
                    id="inputEmail"
                    placeholder="Text"
                    type="text"
                    name="login"
                    onChange={event => {
                      this.setState({ login: event.target.value });
                    }}
                    autoFocus
                    required
                  />
                  <label htmlFor="inputEmail">Username</label>
                  <div className="invalid-feedback">
                    Please provide a valid Username.
                  </div>
                </div>
              </div>
              <div className="form-group">
                <div className="form-label-group">
                  <input
                    type="password"
                    className={
                      "form-control " +
                      (this.state.authError ? "is-invalid" : "")
                    }
                    id="inputPassword"
                    placeholder="******"
                    name="password"
                    onChange={event => {
                      this.setState({ password: event.target.value });
                    }}
                    required
                  />
                  <label htmlFor="inputPassword">Password</label>
                  <div className="invalid-feedback">
                    Please provide a valid Password.
                  </div>
                </div>
              </div>

              <div className="form-group">
                <button
                  className="btn btn-primary btn-block"
                  type="submit"
                  disabled={this.state.isLoading ? true : false}
                >
                  Login &nbsp;&nbsp;&nbsp;
                  {isLoading ? (
                    <span
                      className="spinner-border spinner-border-sm"
                      role="status"
                      aria-hidden="true"
                    ></span>
                  ) : (
                    <span></span>
                  )}
                </button>
              </div>
            </form>
            <div className="text-center">
              <Link className="d-block small mt-3" to={"register"}>
                Register an Account
              </Link>
            </div>
          </div>
        </div>
        {this.renderRedirect()}
      </div>
    );
  }
}
