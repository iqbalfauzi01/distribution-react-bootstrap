import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class Sidebar extends Component {
  render() {
    return (
      <div id="wrapper">
        <ul className="sidebar navbar-nav">
          <li className="nav-item active">
            <Link to={"/dashboard"} className="nav-link">
              {/* <i className="fas fa-fw fa-tachometer-alt"></i> */}
              <span>&nbsp;Dashboard</span>
            </Link>
            {/* <Link className="nav-link" to="/bank">
              <span>&nbsp;Bank</span>
            </Link>
            <Link className="nav-link" to="/brand">
              <span>&nbsp;Brand</span>
            </Link>
            <Link className="nav-link" to="/customers">
              <span>&nbsp;Customers</span>
            </Link>
            <Link className="nav-link" to="/gudang">
              <span>&nbsp;Gudang</span>
            </Link>
            <Link className="nav-link" to="/kategori">
              <span>&nbsp;Kategori</span>
            </Link>
            <Link className="nav-link" to="/pasar">
              <span>&nbsp;Pasar</span>
            </Link>
            <Link className="nav-link" to="/produk">
              <span>&nbsp;Produk</span>
            </Link>
            <Link className="nav-link" to="/sales">
              <span>&nbsp;Sales</span>
            </Link>
            <Link className="nav-link" to="/satuan">
              <span>&nbsp;Satuan</span>
            </Link>
            <Link className="nav-link" to="/Supplier">
              <span>&nbsp;Supplier</span>
            </Link>
            <Link className="nav-link" to="/tipeBayar">
              <span>&nbsp;Tipe Bayar</span>
            </Link>
            <Link className="nav-link" to="/tipeProduk">
              <span>&nbsp;Tipe Produk</span>
            </Link> */}
          </li>

          {/* LINK DROPDOWN */}

          <li className="nav-item dropdown">
            <Link
              className="nav-link dropdown-toggle"
              to={""}
              id="pagesDropdown"
              role="button"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              <i className="fas fa-fw fa-folder"></i>
              <span>&nbsp;List</span>
            </Link>
            <div className="dropdown-menu" aria-labelledby="pagesDropdown">
              <Link className="dropdown-item" to="/bank">
                <span>&nbsp;Bank</span>
              </Link>
              <Link className="dropdown-item" to="/brand">
                <span>&nbsp;Brand</span>
              </Link>
              <Link className="dropdown-item" to="/customers">
                <span>&nbsp;Customers</span>
              </Link>
              <Link className="dropdown-item" to="/gudang">
                <span>&nbsp;Gudang</span>
              </Link>
              <Link className="dropdown-item" to="/kategori">
                <span>&nbsp;Kategori</span>
              </Link>
              <Link className="dropdown-item" to="/pasar">
                <span>&nbsp;Pasar</span>
              </Link>
              <Link className="dropdown-item" to="/produk">
                <span>&nbsp;Produk</span>
              </Link>
              <Link className="dropdown-item" to="/sales">
                <span>&nbsp;Sales</span>
              </Link>
              <Link className="dropdown-item" to="/satuan">
                <span>&nbsp;Satuan</span>
              </Link>
              <Link className="dropdown-item" to="/Supplier">
                <span>&nbsp;Supplier</span>
              </Link>
              <Link className="dropdown-item" to="/tipeBayar">
                <span>&nbsp;Tipe Bayar</span>
              </Link>
              <Link className="dropdown-item" to="/tipeProduk">
                <span>&nbsp;Tipe Produk</span>
              </Link>
              {/* 
              <a className="dropdown-item" href="register.html">Register</a>
              <a className="dropdown-item" href="forgot-password.html">Forgot Password</a>
              <div className="dropdown-divider"></div>
              <h6 className="dropdown-header">Other Pages:</h6>
              <a className="dropdown-item" href="404.html">404 Page</a>
              <a className="dropdown-item" href="blank.html">Blank Page</a> */}
            </div>
          </li>
          {/* <li className="nav-item">
            <Link to={"/index"} className="nav-link">
              <i className="fas fa-fw fa-chart-area"></i>
              <span>&nbsp;CRUD App</span>
            </Link>
          </li> */}
          {/* link to brand */}
          {/* <li className="nav-item">
            <Link to={"/brand"} className="nav-link">
              <i className="fas fa-fw fa-chart-area"></i>
              <span>&nbsp;Brand</span>
            </Link>
          </li> */}
          {/* <li className="nav-item">
            <Link to={"/fileupload"} className="nav-link">
              <i className="fas fa-fw fa-file-archive"></i>
              <span>&nbsp;File Upload</span>
            </Link>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="tables.html">
              <i className="fas fa-fw fa-table"></i>
              <span>&nbsp;Tables</span>
            </a>
          </li> */}
        </ul>
      </div>
    );
  }
}
